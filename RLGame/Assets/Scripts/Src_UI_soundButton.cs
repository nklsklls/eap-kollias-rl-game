﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class Src_UI_soundButton : MonoBehaviour
{
    public AudioMixer masterMixer;
    public Image img;
    private bool musicOff;

    void Start()
    {
        Src_Constants.musicOff = PlayerPrefs.GetInt("sound", 1) == 0;
        UpdateBtn();
    }

    public void UpdateBtn()
    {
        masterMixer.SetFloat("MasterVolume", Src_Constants.musicOff ? -80f : 0f);
        img.sprite = Resources.Load<Sprite>(Src_Constants.musicOff ? "Png/soundOff" : "Png/soundOn");
    }

    private void Update()
    {
        if (Src_Constants.musicOff != musicOff) UpdateBtn();
    }

    public void OnSoundBtnClicked()
    {
        if (!Src_Constants.musicOff) //on -> off
        {
            masterMixer.DOSetFloat("MasterVolume", -80f, 0.4f);
            PlayerPrefs.SetInt("sound", 0);
            Src_Constants.musicOff = musicOff = true;
            img.sprite = Resources.Load<Sprite>("Png/soundOff");
        }
        else //off -> on
        {
            masterMixer.DOSetFloat("MasterVolume", 0f, 0.4f);
            PlayerPrefs.SetInt("sound", 1);
            Src_Constants.musicOff = musicOff = false;
            img.sprite = Resources.Load<Sprite>("Png/soundOn");
        }
    }
}