﻿using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class Src_ToastPanel : MonoBehaviour
{
    [SerializeField] GameObject wToastPanel;
    [SerializeField] CanvasGroup wToastCanvasGroup;
    [SerializeField] Text wToastTxt;
    private IEnumerator makeMeDissapear;

    private void Start()
    {
        //Dont put code here!
    }

    public void showToast(string toastText)
    {
        wToastPanel.SetActive(true);
        if (wToastCanvasGroup.alpha != 0)
        {
            wToastTxt.text = toastText;
            if (makeMeDissapear != null) StopCoroutine(makeMeDissapear);
        }
        else
        {
            wToastCanvasGroup.DOFade(1, 0.445f).SetEase(Ease.OutSine)
                .OnStart(
                    () =>
                    {
                        wToastCanvasGroup.alpha = 0;
                        wToastTxt.text = toastText;
                    }
                );
        }
        makeMeDissapear = MakeMeDissapear(2);
        StartCoroutine(makeMeDissapear);
    }

    IEnumerator MakeMeDissapear(int waitSec)
    {
        yield return new WaitForSeconds(waitSec);
        wToastCanvasGroup.DOFade(0, 0.299f).SetEase(Ease.OutSine)
        .OnComplete(
            () =>
            {
                wToastPanel.SetActive(false);
                wToastTxt.text = "";
                makeMeDissapear = null;
            }
        );
    }

    public void kill()
    {
        if (makeMeDissapear != null) StopCoroutine(makeMeDissapear);
        makeMeDissapear = MakeMeDissapear(0);
        StartCoroutine(makeMeDissapear);
    }
}
