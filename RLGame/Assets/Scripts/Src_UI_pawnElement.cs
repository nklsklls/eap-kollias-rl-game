﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class Src_UI_pawnElement : MonoBehaviour
{
    public GameObject parent;
    public Image wImage;
    public CanvasGroup wCanvasGroup;
    public bool white;
    public bool alive = true;
    public ParticleSystem wParticleSystem;

    public int id;
    public int boardSquareId;
    public int row;
    public int column;

    public AudioSource audioExplosion;
    
    void Start()
    {
        wParticleSystem.gameObject.SetActive(false);
        wCanvasGroup.alpha = 0;
        wImage.sprite = Resources.Load<Sprite>(white ? "Png/player1" : "Png/player2");
    }

    public void checkVisibility()
    {
        // if (alive && wCanvasGroup.alpha == 0){
        //     show();
        // }
    }

    public Tween show()
    {
        return wCanvasGroup.DOFade(1, wCanvasGroup.alpha == 1 ? 0 : 0.234f).SetEase(Ease.OutSine);
    }

    public Tween scaleMe(bool up)
    {
        if (up)
            return parent.transform.DOScale(1.7f, 0.333f).SetEase(Ease.OutSine);
        else
            return parent.transform.DOScale(1, 0.222f).SetEase(Ease.OutSine);
    }

    internal Tween destroyMe(bool isInBase)
    {
        if (!alive) return wCanvasGroup.DOFade(1, 0);
        alive = false;
        Debug.Log("DESTROY " + id + " " + boardSquareId);
        return wCanvasGroup.DOFade(0, 0.433f).SetEase(Ease.OutSine).SetDelay(0.333f)
            .OnStart(
                () =>
                {
                    audioExplosion.Play();
                    boardSquareId = -1;
                    row = column = -1;
                    alive = false;
                    wParticleSystem.gameObject.SetActive(true);
                }
            )
            .OnComplete(
                () => { wParticleSystem.gameObject.SetActive(false); }
            );
    }

    internal void reset()
    {
        alive = true;
    }

    public Tween resetToBeginingState(Vector3 position)
    {
        alive = true;
        return wCanvasGroup.DOFade(0, 0.234f)
            .SetEase(Ease.OutSine)
            .OnComplete(
                () => { parent.GetComponent<RectTransform>().position = position; }
            );
    }
}