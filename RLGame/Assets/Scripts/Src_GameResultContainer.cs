﻿using System;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Src_GameResultContainer : MonoBehaviour
{
    public GameObject parent;
    public CanvasGroup wCanvasGroup;
    public CanvasGroup wBtnsCanvasGroup;
    public Image wWinImageTop;
    public Image wWinImageBottom;
    public Button wReplayBtn;
    public AudioSource audioWin;
    public AudioSource audioLose;
    private Sequence breathRePlayBtnEffect;

    public void ShowWin(bool hasWon)
    {
        parent.SetActive(true);
        wCanvasGroup.alpha = 0;
        if (hasWon)
        {
            wWinImageTop.sprite = Resources.Load<Sprite>("Png/result_win");
            wWinImageBottom.sprite = Resources.Load<Sprite>("Png/result_win_title");
        }
        else
        {
            wWinImageTop.sprite = Resources.Load<Sprite>("Png/result_lose");
            wWinImageBottom.sprite = Resources.Load<Sprite>("Png/result_lose_title");
        }

        wCanvasGroup.DOFade(1, 0.667f).SetEase(Ease.OutSine).SetDelay(2f).OnStart(
            () =>
            {
                //Play Sound
                (hasWon ? audioWin : audioLose).Play();

                Sequence showSeq = DOTween.Sequence();
                showSeq.Join(wWinImageTop.transform.DOScale(0f, 0));
                showSeq.Join(wBtnsCanvasGroup.DOFade(0, 0));
                showSeq.Join(wWinImageBottom.GetComponent<Image>().DOFade(0, 0));
                showSeq.Append(wWinImageTop.transform.DOScale(1, 0.433f).SetEase(Ease.InSine));
                showSeq.Append(wWinImageBottom.GetComponent<Image>().DOFade(1, 0.433f).SetEase(Ease.InSine));
                showSeq.Append(wBtnsCanvasGroup.DOFade(1, 0.333f).SetEase(Ease.InSine));

                if (breathRePlayBtnEffect == null)
                {
                    breathRePlayBtnEffect = DOTween.Sequence();
                    breathRePlayBtnEffect.Join(wReplayBtn.transform.DOScale(1.08f, 0.77f).SetEase(Ease.OutSine));
                    breathRePlayBtnEffect.Append(wReplayBtn.transform.DOScale(1f, 0.77f).SetEase(Ease.InSine));
                    breathRePlayBtnEffect.SetLoops(-1);
                }
                breathRePlayBtnEffect.Play();
            }
        );
    }

    public void onSelectGameModeBtnClicked()
    {
        CloseGameResultContainer();
    }

    public void onReplayBtnClicked()
    {
        CloseGameResultContainer();
    }

    private void CloseGameResultContainer()
    {
        wCanvasGroup.DOFade(0, 0.333f).SetEase(Ease.OutSine).SetDelay(0.15f)
            .OnStart(() => { breathRePlayBtnEffect.Pause(); })
            .OnComplete(
                () => { parent.SetActive(false); }
            );
    }
}