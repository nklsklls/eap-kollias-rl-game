﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LoginCommand : srcCommand
{
    public string userName ;
    public string password ;

    public LoginCommand(string userName, string password, int id, int userId)
    {
        this.userName = userName;
        this.password = password;
        this.id = id;
        this.userId = userId;
        this.setType("gr.eap.RLGameEcoServer.comm.LoginCommand");
    }
}
