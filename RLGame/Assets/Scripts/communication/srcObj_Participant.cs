﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class srcObj_Participant
{
    public enum Role
    {
        NONE, WHITEPLAYER, BLACKPLAYER, SPECTATOR
    }

    public string name;
    public Role role;
    public List<srcObj_Player> players;
    public srcObj_Player teamLeader;
    public List<srcObj_Move> pendingMoves;
}