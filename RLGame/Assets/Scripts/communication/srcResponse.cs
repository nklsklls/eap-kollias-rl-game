﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class srcResponse
{
    public int commandID;
    public ConnectionState connectionState;
    public enum ConnectionState
    {
        CONNECTED, LOGGED_IN, IN_GAME, DISCONNECTED
    }

    public int userId;
    public string type;

    ///////////////////////////////
    // protected void setType(string type)
    // {
    //     this.type = type;
    // }

}
