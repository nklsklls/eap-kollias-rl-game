﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InviteCommand : srcCommand
{
    //{"boardSize":6,"baseSize":2,"numberOfPawns":5,"id":2,"userId":1,"invitedPlayerId":2,"type":"InviteCommand"}
    public int boardSize;
    public int baseSize;
    public int numberOfPawns;
    public int invitedPlayerId;
    public string eGreedy;
    public string gamma;
    public string lambda;
    public int miniMaxPlies;
    public int playerType;

    public InviteCommand(int invitedPlayerId)
    {
        this.boardSize = Src_Constants.boardSize;
        this.baseSize = Src_Constants.baseSize;
        this.numberOfPawns = Src_Constants.numberOfPawns;
        this.invitedPlayerId = invitedPlayerId;
        this.eGreedy = Src_Constants.eGreedy.ToString("F2");
        this.gamma = Src_Constants.gamma.ToString("F2");
        this.lambda = Src_Constants.lambda.ToString("F2");
        this.miniMaxPlies = Src_Constants.miniMaxPlies;
        this.playerType = Src_Constants.playerType;
        this.id = 2;
        this.userId = 1;
        this.setType("gr.eap.RLGameEcoServer.comm.InviteCommand");
    }

    // public InviteCommand(int invitedPlayerId, double _eGreedy, double _gamma, double _lambda)
    // {
    //     this.boardSize = Src_Constants.boardSize;
    //     this.baseSize = Src_Constants.baseSize;
    //     this.numberOfPawns = Src_Constants.numberOfPawns;
    //     this.invitedPlayerId = invitedPlayerId;
    //     this.eGreedy = _eGreedy;
    //     this.gamma = _gamma;
    //     this.lambda = _lambda;
    //     this.id = 2;
    //     this.userId = 1;
    //     this.setType("InviteCommand");
    // }
}
