﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class srcObj_GameState
{
    public int numberOfPawns;
    public int boardSize;
    public int baseSize;
    public int maxNumberOfPawnMoves;
    public int neuralInputSize;
    public int turn;

    public List<srcObj_Pawn> whitePawns;
    public List<srcObj_Pawn> blackPawns;

    //public List<srcObj_Pawn> cloneWhite;
    //public List<srcObj_Pawn> cloneBlack;
    //public List<List<srcObj_Square>> gameBoard;
    //public List<List<srcObj_Square>> cloneGameBoard;

    // Added by Dockos for saving the coordinates of deleted pawns
    //public string positionOfDeletedPawns;

    //public List<double> networkInput;
}
