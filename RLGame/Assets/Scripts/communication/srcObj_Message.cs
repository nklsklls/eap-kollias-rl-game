﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class srcObj_Message
{
    public enum Type
    {
        SYSTEM_INFO, SYSTEM_WARNING, SYSTEM_ALERT, SYSTEM_INTERNAL, USER_BROADCAST, USER_PERSONAL, USER_GAME, USER_TEAM, NONE
    }

    public string text;
    public Type type;
    public srcObj_Player sender;
    public List<srcObj_Player> playersList;
    public List<srcObj_Player> recipients;
}
