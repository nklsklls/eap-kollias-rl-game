﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GamesListResponse 
{
    public List<srcObj_Game> gamesList;
}
