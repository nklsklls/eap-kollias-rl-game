﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class srcCommand
{
    public int id;
    public int userId;
    public string type;

    public void setType(string type)
    {
        this.type = type;
    }

}

