﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class srcObj_Pawn
{
    public int id; // pawn id
    public bool white; // belongs to white??
    public bool alive; // is alive??
    public srcObj_Square position; // the square where this pawn resides
    public int boardSize;
    public int baseSize;
    public int maxNumberOfPawnMoves;

    public srcObj_Square whiteBase;
    public srcObj_Square blackBase;
}
