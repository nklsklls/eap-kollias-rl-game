﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class srcObj_Square
{
    public int xCoord;
    public int yCoord;

    public bool isFree; // no pawn is on that square
    public bool isInWBase;
    public bool isInBBase;
    public int boardSize;
    public int baseSize;
}