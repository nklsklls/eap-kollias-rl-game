﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfirmStartGameCommand : srcCommand
{
    public string gameUid;

    public ConfirmStartGameCommand(string gameUid)
    {
        this.gameUid = gameUid;
        this.id = 4;
        this.userId = 1;
        this.setType("gr.eap.RLGameEcoServer.comm.ConfirmStartGameCommand");
    }
    //{"gameUid":"22615ee2-9aa0-4866-83eb-5a918d037162","id":2,"userId":1,"type":"ConfirmStartGameCommand"}
}
