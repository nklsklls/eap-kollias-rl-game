﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeaveGameCommand : srcCommand
{
    public string gameUid;

    public LeaveGameCommand(string gameUid)
    {
        this.gameUid = gameUid;
        this.id = 2;
        this.userId = 1;
        this.setType("gr.eap.RLGameEcoServer.comm.LeaveGameCommand");
    }
}
