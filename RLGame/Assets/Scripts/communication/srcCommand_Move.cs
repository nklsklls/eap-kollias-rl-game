﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MoveCommand : srcCommand
{
    public int pawnId;
    public int toXCoord;
    public int toYCoord;
    public string gameUid;

    public MoveCommand(int pawnId, int toXCoord, int toYCoord, string gameUid)
    {
        this.pawnId = pawnId;
        this.toXCoord = toXCoord;
        this.toYCoord = toYCoord;
        this.gameUid = gameUid;
        this.id = 2;
        this.userId = 1; ;
        this.setType("gr.eap.RLGameEcoServer.comm.MoveCommand");
    }
}
