﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MessageResponse
{
    public int commandID;
    public ConnectionState connectionState;
    public enum ConnectionState
    {
        CONNECTED, LOGGED_IN, IN_GAME, DISCONNECTED
    }

    public int userId;
    public string type;
    public srcObj_Message message;

}
