﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameStateResponse : srcResponse
{
    public srcObj_GameState state;
    public string gameUid;

}
