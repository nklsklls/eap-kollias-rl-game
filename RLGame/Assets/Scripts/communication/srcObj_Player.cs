﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class srcObj_Player
{
    public string userName;
    public string password;
    public ConnectionState connectionState;
    public int score;
    public int id;
    public string name;
    public bool isHuman;

    public enum ConnectionState
    {
        CONNECTED, LOGGED_IN, IN_GAME, DISCONNECTED
    }
}
