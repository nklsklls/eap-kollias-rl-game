﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class srcObj_Game
{
    public string uid;
    public string startDateTime;
    public string duration;
    public List<srcObj_Participant> participants;
    public int boardSize;
    public int baseSize;
    public int numberOfPawns;
    public GameStatus status;
    public bool whitePlayerReady = false;
    public bool blackPlayerReady = false;

    // whitePlayer and player2 properties will be read-only and will get updated
    // when needed, so that we can correctly serialize those properties
    public srcObj_Participant whitePlayer;
    public srcObj_Participant blackPlayer;
    public srcObj_Participant spectator;


    

    public enum GameStatus
    {
        WAITING_FOR_PLAYERS, IN_PROGRESS, INTERRUPTED, FINISHED
    }


    
}
