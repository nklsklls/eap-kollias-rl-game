﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoinGameCommand : srcCommand
{
    // Start is called before the first frame update
    public srcObj_Participant.Role role;
    public string gameUid;

    public JoinGameCommand(string gameUid, srcObj_Participant.Role role)
    {
        this.gameUid = gameUid;
        this.role = role;
        this.id = 2;
        this.userId = 1;
        this.setType("gr.eap.RLGameEcoServer.comm.JoinGameCommand");
    }
    //{"gameUid":"22615ee2-9aa0-4866-83eb-5a918d037162","id":2,"userId":1,"type":"ConfirmStartGameCommand"}
}
