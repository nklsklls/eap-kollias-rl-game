﻿using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class Src_SplashAndMenuScreen : MonoBehaviour
{
    [Header("Splash")] 
    public GameObject splashPanel;
    public CanvasGroup splashCanvasGroup;
    public GameObject splashLogo1;
    public GameObject splashLogo2;

    [Header("Lobby")] public GameObject menuPanel;
    public CanvasGroup menuCanvasGroup;
    public List<Text> tutorialText;
    private Sequence showMenuSequence;
    
    public void ShowSplash()
    {
        splashPanel.SetActive(true);
        menuPanel.SetActive(true);
        splashCanvasGroup.alpha = 0f;
        menuCanvasGroup.alpha = 0;
        splashLogo2.transform.DOScale(0, 0f);

        Vector3 logo1Pos = splashLogo1.transform.position;
        splashLogo1.transform.position = new Vector3(0, -20, 1);

        Sequence introSequence = DOTween.Sequence();
        introSequence.Join(splashCanvasGroup.DOFade(1, 0.333f).SetEase(Ease.OutSine));
        introSequence.Append(splashLogo1.transform.DOMove(logo1Pos, 0.633f).SetEase(Ease.InOutSine));
        introSequence.Append(splashLogo2.transform.DOScale(1, 0.533f).SetEase(Ease.InSine));
        introSequence.Append(splashLogo2.transform.DOShakeRotation(1.7f, 19f));
        introSequence.Join(splashLogo1.transform.DOScale(1.08f, 1.7f).SetEase(Ease.InSine));

        introSequence.Append(splashLogo1.transform.DOMove(new Vector3(-20, 0, 1), 0.333f).SetEase(Ease.InSine));
        introSequence.Join(splashLogo2.transform.DOMove(new Vector3(20, 0, 1), 0.333f).SetEase(Ease.InSine));
        introSequence.Join(splashCanvasGroup.DOFade(0, 0.333f).SetEase(Ease.OutSine));

        introSequence.OnComplete(
            () =>
            {
                splashPanel.SetActive(false);
                menuCanvasGroup.alpha = 0;
                ShowMenu();
            }
        );
    }
    
    public void ShowMenu()
    {
        if (menuPanel.active && menuCanvasGroup.alpha == 1) return;
        menuPanel.SetActive(true);
        menuCanvasGroup.alpha = 0;
        menuCanvasGroup.DOFade(1, 0.333f).SetEase(Ease.OutSine);
        //
        if (showMenuSequence == null)
        {
            showMenuSequence = DOTween.Sequence();
            showMenuSequence.SetDelay(1f);
            for (int i = 0; i < tutorialText.Count; i++)
            {
                showMenuSequence.Append(tutorialText[i].gameObject.GetComponent<RectTransform>().DOScale(1.16f, .333f).SetEase(Ease.OutSine).SetDelay(0.6f));
                showMenuSequence.Append(tutorialText[i].gameObject.GetComponent<RectTransform>().DOScale(1f, .333f).SetEase(Ease.OutSine).SetDelay(0.8f));
            }
        }
        else
        {
            showMenuSequence.Restart();
        }
    }

    public void HideMenu()
    {
        menuCanvasGroup.DOFade(0, 0.333f).SetEase(Ease.OutSine)
            .OnComplete(
                () =>
                {
                    showMenuSequence.Pause();
                    menuPanel.SetActive(false);
                }
            );
    }
}