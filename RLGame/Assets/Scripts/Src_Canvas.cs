﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using NativeWebSocket;

public class Src_Canvas : MonoBehaviour
{
    [Header("Server Settings")] [SerializeField]
    public bool useLocalServer = true; //"ws://localhost:3888";

    [SerializeField] public string localServerUrl = "ws://127.0.0.1:3888"; //"ws://localhost:3888";
    [SerializeField] public string remoteServerUrl = "ws://nklsk.xyz:3888"; //"ws://localhost:3888";
    [SerializeField] public string username = "n1";
    [SerializeField] public string password = "0";

    [Header("RL Settings")] [SerializeField]
    public int miniMaxDepth = 3;

    [SerializeField] public float eGreedy = 0.7f;
    [SerializeField] public float gamma = 0.95f;
    [SerializeField] public float lambda = 0.5f;

    [Header("Debug Settings")] [SerializeField]
    public bool showStartAnimation = true;

    [SerializeField] public bool debugServerMessages = false;
    [SerializeField] public bool showGridSquareId = false;

    [Header("SubGameObjects")] [SerializeField]
    Src_GameController wGameController;

    [SerializeField] Src_Communication srcCommunication;
    [SerializeField] Src_GameResultContainer srcGameResultContainer;
    [SerializeField] Src_ToastPanel srcToastPanel;
    [SerializeField] Src_SplashAndMenuScreen srcSplashAndMenuContainer;
    [SerializeField] GameObject rlGameParametersPanel;
    [SerializeField] GameObject creditsPanel;

    //This queue is used to exectute things on MainThread
    private static Queue<Action> ExecutionQ = new Queue<Action>();

    //Timer for backpress
    private long wBackPressed;

    //Timer to add delay between two gameStateResponses
    private long lastProccessedGameState;

    void Start()
    {
        Src_Constants.useLocalServer = useLocalServer;
        wGameController.wMoveEvent.AddListener((x, y, z) => { MovePawnToServer(x, y, z); });
        srcCommunication.wProccessGameStateEvent.AddListener((x) => { proccessGameState(x); });
        srcCommunication.wProccessMessageResponseEvent.AddListener((y) => { proccessMessageResponse(y); });
        srcCommunication.wSocketEvent.AddListener((y) => { proccessSocketEvent(y); });
        //
        if (!Debug.isDebugBuild)
        {
            showStartAnimation = true; //set showStartAnimation to true if game is at production state
            debugServerMessages = false;
        }

        if (showStartAnimation)
            srcSplashAndMenuContainer.ShowSplash();
        else
            srcSplashAndMenuContainer.ShowMenu();
        //
        wGameController.wBoardPanel.SetActive(false);
        rlGameParametersPanel.SetActive(false);
        Src_Constants.showGridSquareId = showGridSquareId;
        Src_Constants.miniMaxPlies = miniMaxDepth;
        Src_Constants.eGreedy = eGreedy;
        Src_Constants.gamma = gamma;
        Src_Constants.lambda = lambda;
    }

    void Update()
    {
        while (ExecutionQ.Count != 0)
        {
            ExecutionQ.Dequeue().Invoke();
        }

        //
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (wBackPressed + 25000000 > DateTime.Now.Ticks)
            {
                StartCoroutine(ExitApp(1.3f));
            }
            else
            {
                srcToastPanel.showToast("Press back again \nto exit game.");
            }

            wBackPressed = DateTime.Now.Ticks;
        }
    }

    IEnumerator ExitApp(float time)
    {
        yield return new WaitForSeconds(time);
        Application.Quit();
    }

    #region Debug Buttons

    public void onBtnCloseCommunication()
    {
        srcCommunication.close();
    }

    public void onBtnResetGame()
    {
        StartCoroutine(ReStartGame());
    }

    #endregion

    public void startGameButton(int id)
    {
        Src_Constants.playerType = id;

        srcSplashAndMenuContainer.HideMenu();
        wGameController.show();
        //
        StartCoroutine(StartConnection());
    }

    IEnumerator StartConnection()
    {
        //yield return new WaitForSeconds(1.2f);
        srcCommunication.startConnection();
        yield return new WaitForSeconds(0.888f);
        StartCoroutine(ReStartGame());
    }

    public void onSelectGameModeBtnClicked()
    {
        wGameController.hide();
        wGameController.resetGridAndPawns();
        srcSplashAndMenuContainer.ShowMenu();
    }

    public void onReplayBtnClicked()
    {
        StartCoroutine(StartGame());
    }

    public void onGoBackFromGameClicked()
    {
        StartCoroutine(LeaveGame());
    }

    private IEnumerator StartGame()
    {
        srcCommunication.sendInviteCommand();
        yield return new WaitForSeconds(0.6f);
        srcCommunication.sendStartGameCommand();
    }

    private IEnumerator ReStartGame()
    {
        //wSrcCommunication.sendLeaveGameCommand();
        yield return new WaitForSeconds(0.6f);
        srcCommunication.sendInviteCommand();
        yield return new WaitForSeconds(0.6f);
        srcCommunication.sendStartGameCommand();
    }

    private IEnumerator LeaveGame()
    {
        onSelectGameModeBtnClicked();
        srcCommunication.sendLeaveGameCommand();
        yield return new WaitForSeconds(0.6f);
    }

    void MovePawnToServer(int pawnId, int toXCoord, int toYCoord)
    {
        //Debug.Log("MovePawnToServer" + pawnId + " " + toXCoord + " " + toYCoord);
        srcCommunication.sendMoveCommand(pawnId, toXCoord, toYCoord);
    }

    private void proccessGameState(GameStateResponse gameStateResponse)
    {
        long diff = DateTime.Now.Ticks - lastProccessedGameState;
        //Debug.Log("add time: " + diff + (diff < 1000000?" +++ " : ""));
        StartCoroutine(AddDelayToGameStateProccess(diff < 1000000 ? 0.611f: 0 , gameStateResponse));
    }

    IEnumerator AddDelayToGameStateProccess(float time, GameStateResponse gameStateResponse)
    {
        yield return new WaitForSeconds(time);
        lastProccessedGameState = DateTime.Now.Ticks;
        wGameController.proccessGameState(gameStateResponse);
    }

    private void proccessMessageResponse(MessageResponse messageResponse)
    {
        ExecutionQ.Enqueue(() =>
        {
            if (messageResponse.message.text != null)
            {
                if (messageResponse.message.text.Contains("White Player wins"))
                {
                    srcGameResultContainer.ShowWin(true);
                    srcCommunication.sendLeaveGameCommand();
                }
                else if (messageResponse.message.text.Contains("Black Player wins"))
                {
                    srcGameResultContainer.ShowWin(false);
                    srcCommunication.sendLeaveGameCommand();
                }
                else if (messageResponse.message.text.Contains("Login successfull"))
                {
                    //wGameController.updateGrid_WB_PawnsInBase();
                }
                else
                {
                    srcToastPanel.showToast(messageResponse.message.text);
                }
            }
        });
    }

    private void proccessSocketEvent(WebSocketState _webSocketState)
    {
        //Debug.Log("socket status changes: " + _webSocketState);
        if (_webSocketState == WebSocketState.Closed)
        {
            ExecutionQ.Enqueue(() =>
            {
                srcToastPanel.showToast("Disconnected from server :(");
                wGameController.hide();
                srcSplashAndMenuContainer.ShowMenu();
            });
        }
        else if (_webSocketState == WebSocketState.Open)
        {
            //wSrcStartGameContainer.hide();
            //wGameController.show();
        }
    }

    public void onLogoClick_Debug()
    {
        rlGameParametersPanel.SetActive(true);
    }

    public void onShowCreditsButton()
    {
        Sequence showCreditsSequence = DOTween.Sequence();
        showCreditsSequence.Append(creditsPanel.GetComponent<CanvasGroup>().DOFade(1, 0.556f).SetEase(Ease.OutSine)
            .OnStart(() =>
            {
                creditsPanel.GetComponent<CanvasGroup>().alpha = 0;
                creditsPanel.SetActive(true);
            }));
        showCreditsSequence.Join(creditsPanel.GetComponent<CanvasGroup>().DOFade(0, 0.444f).SetEase(Ease.InSine)
            .SetDelay(3.3f)
            .OnComplete(() => { creditsPanel.SetActive(false); }));
    }
}