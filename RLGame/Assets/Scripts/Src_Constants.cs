﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Src_Constants
{
    public static bool showGridSquareId;
    //
    public static bool useLocalServer;
    public static bool musicOff;
    //
    public static Color highlightGridSquarePawn = new Color(1, 0.98f, 0.65f,0.65f);
    public static Color highlightGridSquareStep = new Color(0.9962779f, 1, 0, 0.5f);
    //
    public static int boardSize = 6;
    public static int baseSize = 2;
    public static int numberOfPawns = 5;
    //
    public static double eGreedy;
    public static double gamma;
    public static double lambda;
    public static int miniMaxPlies;
    //
    // public static final int RL_PLAYER = 1;
    // public static final int MM_PLAYER = 2;
    // public static final int RANDOM_PLAYER = 3;
    // public static final int HUMAN_PLAYER = 4;
    public static int playerType;
}
