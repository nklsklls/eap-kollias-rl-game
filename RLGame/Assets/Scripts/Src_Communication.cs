﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using NativeWebSocket;

[System.Serializable]
public class ProccessGameStateEvent : UnityEvent<GameStateResponse>
{
}

[System.Serializable]
public class ProccessMessageResponseEvent : UnityEvent<MessageResponse>
{
}

[System.Serializable]
public class SocketEvent : UnityEvent<WebSocketState>
{
}

public class Src_Communication : MonoBehaviour
{
    public Src_Canvas srcCanvas;
    public ProccessGameStateEvent wProccessGameStateEvent;
    public ProccessMessageResponseEvent wProccessMessageResponseEvent;
    public SocketEvent wSocketEvent;
    private WebSocket wWebSocket;

    // Start is called before the first frame update
    async void Start()
    {
        if (wProccessGameStateEvent == null)
            wProccessGameStateEvent = new ProccessGameStateEvent();
        if (wProccessMessageResponseEvent == null)
            wProccessMessageResponseEvent = new ProccessMessageResponseEvent();
        if (wSocketEvent == null)
            wSocketEvent = new SocketEvent();
    }

    internal async void close()
    {
        wWebSocket.Close();
        StartCoroutine(StartSocket());
    }

    internal async void startConnection()
    {
        if (wWebSocket == null || wWebSocket.State != WebSocketState.Open)
            StartCoroutine(StartSocket());
    }

    IEnumerator StartSocket()
    {
        wWebSocket = new WebSocket(Src_Constants.useLocalServer ? srcCanvas.localServerUrl : srcCanvas.remoteServerUrl);
        wWebSocket.OnOpen += () => { Debug.Log("Socket Open!"); };
        wWebSocket.OnOpen += () => wSocketEvent.Invoke(WebSocketState.Open);
        wWebSocket.OnMessage += (bytes) => proccessServerMessage(System.Text.Encoding.UTF8.GetString(bytes));
        wWebSocket.OnError += (e) => Debug.Log("OnError: " + e);
        wWebSocket.OnClose += (e) => Debug.Log("Socket Close: " + e);
        wWebSocket.OnClose += (e) => wSocketEvent.Invoke(WebSocketState.Closed);

        try
        {
            wWebSocket.Connect();
        }
        catch (InvalidOperationException ex)
        {
            wSocketEvent.Invoke(WebSocketState.Closed); //todo change this event to CannotConnect
        }

        // Keep sending messages at every 0.3s
        InvokeRepeating("SendWebSocketMessage", 0.0f, 0.3f);

        //await websocket.Connect();

        yield return 0;
    }

    public bool isConnected()
    {
        return wWebSocket.State == WebSocketState.Open;
    }

    public void sendLogin()
    {
        if (!isConnected()) return;
        LoginCommand loginCommand = new LoginCommand(srcCanvas.username, srcCanvas.password, 1, 0);
        wWebSocket.SendText(JsonUtility.ToJson(loginCommand));
        if (srcCanvas.debugServerMessages) Debug.Log("OUT: " + JsonUtility.ToJson(loginCommand));
    }

    public void sendInviteCommand()
    {
        if (!isConnected()) return;
        InviteCommand inviteCommand = new InviteCommand(2);
        wWebSocket.SendText(JsonUtility.ToJson(inviteCommand));
        if (srcCanvas.debugServerMessages) Debug.Log("OUT: " + JsonUtility.ToJson(inviteCommand));
    }

    public void sendJoinCommand()
    {
        if (!isConnected()) return;
        JoinGameCommand joinGameCommand =
            new JoinGameCommand(gamesListResponse.gamesList[0].uid, srcObj_Participant.Role.WHITEPLAYER);
        wWebSocket.SendText(JsonUtility.ToJson(joinGameCommand));
        if (srcCanvas.debugServerMessages) Debug.Log("OUT: " + JsonUtility.ToJson(joinGameCommand));
    }

    public void sendStartGameCommand()
    {
        if (!isConnected()) return;
        ConfirmStartGameCommand confirmStartGameCommand =
            new ConfirmStartGameCommand(gamesListResponse.gamesList[0].uid);
        wWebSocket.SendText(JsonUtility.ToJson(confirmStartGameCommand));
        if (srcCanvas.debugServerMessages) Debug.Log("OUT: " + JsonUtility.ToJson(confirmStartGameCommand));
    }

    internal void sendLeaveGameCommand()
    {
        if (!isConnected()) return;
        if (gamesListResponse == null || gamesListResponse.gamesList.Count == 0) return;
        //if (gamesListResponse == null || gamesListResponse.gamesList == null) return;
        LeaveGameCommand leaveGameCommand = new LeaveGameCommand(gamesListResponse.gamesList[0].uid);
        wWebSocket.SendText(JsonUtility.ToJson(leaveGameCommand));
        if (srcCanvas.debugServerMessages) Debug.Log("OUT: " + JsonUtility.ToJson(leaveGameCommand));
    }

    public void sendMoveCommand(int pawnId, int toXCoord, int toYCoord)
    {
        if (!isConnected()) return;
        MoveCommand moveCommand = new MoveCommand(pawnId, toXCoord, toYCoord, gamesListResponse.gamesList[0].uid);
        wWebSocket.SendText(JsonUtility.ToJson(moveCommand));
        if (srcCanvas.debugServerMessages) Debug.Log("OUT: " + JsonUtility.ToJson(moveCommand));
    }

    /// // / /////
    private GamesListResponse gamesListResponse;

    private MessageResponse messageResponse;
    private GameStateResponse gameStateResponse;

    public void proccessServerMessage(String reply)
    {
        if (srcCanvas.debugServerMessages) Debug.Log("IN: " + reply);
        if (reply.Contains("MessageResponse"))
        {
            messageResponse = JsonUtility.FromJson<MessageResponse>(reply);
            if (messageResponse != null && messageResponse.message != null &&
                (messageResponse.message.text != null && messageResponse.message.text.Equals("Connection Successful")))
            {
                sendLogin();
                wProccessGameStateEvent.AddListener(proccessGameState);
                wProccessMessageResponseEvent.AddListener(proccessMessageResponse);
                //sendInviteCommand();
            }

            wProccessMessageResponseEvent.Invoke(messageResponse);
        }
        else if (reply.Contains("GamesListResponse"))
        {
            gamesListResponse = JsonUtility.FromJson<GamesListResponse>(reply);
            //Debug.Log("gamesListResponse: " + gamesListResponse.gamesList.ToString());
        }
        else if (reply.Contains("GameStateResponse"))
        {
            gameStateResponse = JsonUtility.FromJson<GameStateResponse>(reply);
            //Debug.Log("gamesListResponse: " + gamesListResponse.gamesList.ToString());
            wProccessGameStateEvent.Invoke(gameStateResponse);
        }
    }

    private void proccessGameState(GameStateResponse gameStateResponse)
    {
    }

    private void proccessMessageResponse(MessageResponse messageResponse)
    {
    }

    void Update()
    {
#if !UNITY_WEBGL || UNITY_EDITOR
        if (wWebSocket != null) wWebSocket.DispatchMessageQueue();
#endif
    }

    async void SendWebSocketMessage()
    {
        if (wWebSocket.State == WebSocketState.Open)
        {
            // Sending bytes
            await wWebSocket.Send(new byte[] {10, 20, 30});

            // Sending plain text
            await wWebSocket.SendText("plain text message");
        }
    }

    private async void OnApplicationQuit()
    {
        await wWebSocket.Close();
    }
}