﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class Src_UI_gridElement : MonoBehaviour
{
    public Image wHighlightImage;
    public CanvasGroup wHighlightImageCanvasGroup;
    public Button wButton;
    public Text wDebugTxt;
    public bool isBase = false;
    public int id;
    public int row;
    public int column;

    private Src_UI_gridElement instance;
    private Sequence wHighLightSequense;

    void Start()
    {
        instance = this;
        wButton.interactable = true;
        if (Src_Constants.showGridSquareId)
        {
            wDebugTxt.gameObject.SetActive(true);
            wDebugTxt.text = id + " (" + row + "," + column + ")";
        }
    }

    public void setup(Src_GameController gameController)
    {
        wButton.onClick.AddListener(() =>
            {
                gameController.onSquareClick(instance);
            });
    }

    public Tween highLight(bool highLight)
    {
        if (isBase)
        {
            wHighlightImage.gameObject.SetActive(false);
            return wHighlightImageCanvasGroup.DOFade(0, 0f);
        }

        if (highLight)
        {
            return wHighlightImageCanvasGroup.DOFade(1, 0.333f).SetEase(Ease.OutSine)
                .OnStart(
                    () =>
                    {
                        wHighlightImage.color = Src_Constants.highlightGridSquareStep;
                        wHighlightImageCanvasGroup.alpha = 0;
                        wHighlightImage.gameObject.SetActive(true);
                    }
                ).OnComplete(() =>
                {
                    wHighLightSequense = DOTween.Sequence();
                    wHighLightSequense.Join(wHighlightImageCanvasGroup.DOFade(0.6f, 0.667f).SetEase(Ease.OutSine));
                    wHighLightSequense.Append(wHighlightImageCanvasGroup.DOFade(1, 0.667f).SetEase(Ease.OutSine));
                    wHighLightSequense.SetLoops(-1);
                    wHighLightSequense.Play();
                }
                );
        }
        else
        {
            return wHighlightImageCanvasGroup.DOFade(0, 0.123f).SetEase(Ease.OutSine)
                .OnStart(
                    () =>
                    {
                        wHighLightSequense.Kill();
                    })
               .OnComplete(
                   () =>
                   {
                       wHighlightImage.gameObject.SetActive(false);
                   }
               );
        }
    }

    public Tween highLightHasPawn(bool highLight)
    {
        if (isBase)
        {
            wHighlightImage.gameObject.SetActive(false);
            return wHighlightImageCanvasGroup.DOFade(0, 0f);
        } 
        
        if (highLight)
        {
            return wHighlightImageCanvasGroup.DOFade(1, 0.123f).SetEase(Ease.OutSine)
                 .OnStart(
                     () =>
                     {
                         wHighlightImage.color = Src_Constants.highlightGridSquarePawn;
                         wHighlightImageCanvasGroup.alpha = 0;
                         wHighlightImage.gameObject.SetActive(true);
                     }
                 );
        }
        else
        {
            return wHighlightImageCanvasGroup.DOFade(0, 0.123f).SetEase(Ease.OutSine)
                 .OnComplete(
                     () =>
                     {
                         wHighlightImage.gameObject.SetActive(false);
                     }
                 );
        }
    }
}
