﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[System.Serializable]
public class MyMoveEvent : UnityEvent<int, int, int>
{
}

public class Src_GameController : MonoBehaviour
{
    #region Public vars

    public GameObject wBoardPanel;
    public GameObject wPawnContainer;
    public GameObject wGridContainer;
    public GameObject wPawnElementPrefab;
    public GameObject wGridElementPrefab;
    public Src_UI_baseElement wWhiteBasePanelScript;
    public Src_UI_baseElement wBlackBasePanelScript;
    public MyMoveEvent wMoveEvent;
    public AudioSource wAudioTransition;

    [SerializeField] Text wDebugPawnStatus;

    #endregion

    #region Private vars

    private Src_UI_pawnElement wSelectedPawn;
    private Src_UI_pawnElement[] wListPawnsWhite;
    private Src_UI_pawnElement[] wListPawnsBlack;
    private List<Src_UI_gridElement> wListSquares;

    private bool isSquareClickEnabled = false;
    private int wTotalPawns;
    private int wBoardSize;
    private int wBaseSize;
    private int wBaseIdWhite;
    private int wBaseIdBlack;
    private int wSelectedPawnSquareId = -1;
    private static Queue<Action> ExecutionQ = new Queue<Action>();

    #endregion

    #region Lifecycle

    void Start()
    {
        //show board game is driven from canvas script
        StartCoroutine(CreateGame(6, 2, 5));
        if (wMoveEvent == null)
            wMoveEvent = new MyMoveEvent();
        wMoveEvent.AddListener(SendUserMovePawnToServer);
    }

    public void show()
    {
        wBoardPanel.SetActive(true);
        wBoardPanel.GetComponent<CanvasGroup>().alpha = 0;
        wBoardPanel.GetComponent<CanvasGroup>().DOFade(1, 0.333f).SetEase(Ease.OutSine);
    }

    public void hide()
    {
        wBoardPanel.GetComponent<CanvasGroup>().DOFade(0, 0.333f).SetEase(Ease.OutSine)
            .OnComplete(
                () =>
                {
                    wBoardPanel.SetActive(false);
                    //wMoveEvent.RemoveAllListeners();
                }
            );
    }

    void Update()
    {
        while (ExecutionQ.Count != 0)
        {
            ExecutionQ.Dequeue().Invoke();
        }
    }

    #endregion

    #region SquareClick, HighLightSquares

    internal void onSquareClick(Src_UI_gridElement gridElement)
    {
        if (!isSquareClickEnabled) return;
        //Debug.Log("SquareClick_(id:" + gridElement.id + ")_(" + gridElement.row + "," + gridElement.column + ") isBase:" + gridElement.isBase + " --" + wSelectedPawn);

        //if player clicks in base then highlight the pawn
        if (gridElement.row < wBaseSize && gridElement.column < wBaseSize)
        {
            gridElement = wListSquares[0];
        }

        Src_UI_pawnElement pawn = findPawnInGrid(gridElement.id);
        if (pawn != null && pawn.white)
        {
            pawn.checkVisibility();
            if (wSelectedPawn == null)
            {
                wSelectedPawn = findPawnInGrid(gridElement.id);
                wSelectedPawnSquareId = gridElement.id;
                highlightSquares(gridElement);
            }
            else
            {
                wSelectedPawn = null;
                wSelectedPawnSquareId = -1;
                deHighlightSquares();
                //if (wSelectedPawn != pawn) //add deselect and select pawn
            }
        }
        //Send move cmd to server
        else if (pawn == null && wSelectedPawn != null && wSelectedPawnSquareId >= 0 &&
                 !isSquareBlackOrWhiteBase(wListSquares[gridElement.id].row, wListSquares[gridElement.id].column))
        {
            wMoveEvent.Invoke(wSelectedPawn.id, wListSquares[gridElement.id].row, wListSquares[gridElement.id].column);
        }
        //if any of opp base square is clicked, move pawn to opp base (at the winning)
        else if (gridElement.row >= (wBoardSize - wBaseSize) && gridElement.column >= (wBoardSize - wBaseSize) &&
                 wBlackBasePanelScript.squareRowOppEntersBase != -1)
        {
            wMoveEvent.Invoke(wSelectedPawn.id, wBlackBasePanelScript.squareRowOppEntersBase,
                wBlackBasePanelScript.squareColumnOppEntersBase);
        }
    }

    private void highlightSquares(Src_UI_gridElement clickedGridSquare)
    {
        int selectedCounter = 0;
        Src_UI_pawnElement selectedPawn = findPawnInGrid(clickedGridSquare.id);
        //
        if (selectedPawn != null)
        {
            //Check where are pawns and store them in hasPawn variable  array
            bool[] hasPawn = new bool[wListSquares.Count];
            for (int i = 0; i < wListSquares.Count; i++)
            {
                hasPawn[i] = hasGridSquarePawn(i);
            }

            for (int i = 0; i < wListSquares.Count; i++)
            {
                Src_UI_gridElement _gridSquare = wListSquares[i];
                if (selectedPawn.white && !hasPawn[i]) //User clicked on white pawn (his own)
                {
                    //User clicked a pawn in base, highlight the round the base squares
                    // if (clickedGridSquare.isBase && !_gridSquare.isBase && _gridSquare.row <= wBaseSize && _gridSquare.column <= wBaseSize && !isSquareWhiteBase(_gridSquare.id))
                    // {
                    //     _gridSquare.highLight(true);
                    //     selectedCounter++;
                    // }
                    // else if (!clickedGridSquare.isBase &&
                    //     (((_gridSquare.row == selectedPawn.row - 1 || _gridSquare.row == selectedPawn.row + 1) && _gridSquare.column == selectedPawn.column) ||
                    //     ((_gridSquare.column == selectedPawn.column - 1 || _gridSquare.column == selectedPawn.column + 1) && _gridSquare.row == selectedPawn.row)))
                    // {
                    //     if (!isSquareWhiteBase(_gridSquare.id))
                    //     {
                    //         bool cond1 = Math.Max(selectedPawn.row - wBaseSize, selectedPawn.column - wBaseSize) <=
                    //             Math.Max(selectedPawn.row - wBaseSize, _gridSquare.column - wBaseSize);
                    //         bool cond2 = Math.Max(_gridSquare.row - wBaseSize, selectedPawn.column - wBaseSize) <=
                    //             Math.Max(_gridSquare.row - wBaseSize, selectedPawn.column - wBaseSize);
                    //         if (cond1 && cond2)
                    //         {
                    //             _gridSquare.highLight(true);
                    //             selectedCounter++;
                    //         }
                    //     }
                    // }
                    if (isMoveLegit(_gridSquare))
                    {
                        _gridSquare.highLight(true);
                        _gridSquare.wButton.interactable = true;
                        selectedCounter++;

                        if (_gridSquare.isBase)
                        {
                            wBlackBasePanelScript.highLight(true);
                            wBlackBasePanelScript.squareRowOppEntersBase = _gridSquare.row;
                            wBlackBasePanelScript.squareColumnOppEntersBase = _gridSquare.column;
                        }
                    }
                    else if (_gridSquare.row < wBaseSize && _gridSquare.column < wBaseSize
                    ) //allow click for base squares
                    {
                        _gridSquare.wButton.interactable = true;
                    }
                    else if (_gridSquare.row >= (wBoardSize - wBaseSize) &&
                             _gridSquare.column >= (wBoardSize - wBaseSize)) //allow click opp base squares
                    {
                        _gridSquare.wButton.interactable = true;
                    }
                    else
                    {
                        _gridSquare.wButton.interactable = false;
                    }
                }
            }
        }

        if (selectedCounter > 0) clickedGridSquare.highLightHasPawn(true);
        else Debug.Log("Warning! cannot move!");
    }

    private void deHighlightSquares()
    {
        for (int i = 0; i < wListSquares.Count; i++)
            wListSquares[i].highLight(false);
        wSelectedPawn = null;
        wBlackBasePanelScript.highLight(false);
        getDebugPawnStatus();
    }

    private bool isMoveLegit(Src_UI_gridElement toSquare)
    {
        //First of all the square that the pawn is moved to, should be within board limits
        if (toSquare.row < 0 || toSquare.column < 0 || toSquare.row >= wBoardSize || toSquare.column >= wBoardSize)
            return false;

        int distance1, distance2;
        if (wSelectedPawn.alive)
        {
            if (wSelectedPawn.white)
            {
                if (wSelectedPawn.boardSquareId == wBaseIdWhite)
                {
                    //if (toSquare.row == wBaseSize || toSquare.column == wBaseSize && toSquare.row)
                    if ((toSquare.row == wBaseSize && toSquare.column < wBaseSize) ||
                        (toSquare.column == wBaseSize && toSquare.row < wBaseSize))
                        return true; //toSquare is adjacent to the base
                    else
                        return false; //toSquare is not adjacent to the base
                }
                else if ((
                    wSelectedPawn.row == toSquare.row && Math.Abs(wSelectedPawn.column - toSquare.column) == 1 ||
                    wSelectedPawn.column == toSquare.column && Math.Abs(wSelectedPawn.row - toSquare.row) == 1))
                {
                    distance1 = Math.Max(wSelectedPawn.row + 1 - wBaseSize, wSelectedPawn.column + 1 - wBaseSize);
                    distance2 = Math.Max(toSquare.row + 1 - wBaseSize, toSquare.column + 1 - wBaseSize);
                    return distance2 >= distance1;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (wSelectedPawn.boardSquareId == wBaseIdBlack)
                {
                    if (toSquare.row == wBoardSize - 1 - wBaseSize || toSquare.column == wBoardSize - 1 - wBaseSize)
                        return true; //toSquare is adjacent to the base
                    else
                        return false; //toSquare is not adjacent to the base
                }
                else if ((
                    (wSelectedPawn.row == toSquare.row && (Math.Abs(wSelectedPawn.column - toSquare.column) == 1)) ||
                    (wSelectedPawn.column == toSquare.column && (Math.Abs(wSelectedPawn.row - toSquare.row) == 1))))
                {
                    distance1 = Math.Max(wBoardSize - wBaseSize - wSelectedPawn.row,
                        wBoardSize - wBaseSize - wSelectedPawn.column);
                    distance2 = Math.Max(wBoardSize - wBaseSize - toSquare.row,
                        wBoardSize - wBaseSize - toSquare.column);
                    return distance2 >= distance1;
                }
                else
                {
                    return false;
                }
            }
        }
        else
        {
            return false;
        }
    }

    #endregion

    #region Create Grid and Pawns

    private IEnumerator CreateGame(int boardSize, int baseSize, int numberOfPawns)
    {
        gameEnded = false;
        createGrid(boardSize, baseSize, numberOfPawns);
        yield return new WaitForEndOfFrame();
        wListPawnsWhite = new Src_UI_pawnElement[numberOfPawns];
        wListPawnsBlack = new Src_UI_pawnElement[numberOfPawns];
        //
        ExecutionQ.Enqueue(() =>
        {
            for (int i = 0; i < 5; i++)
            {
                createPawn(true, i);
                createPawn(false, i);
            }
        });
    }

    private void createPawn(bool white, int pawnId)
    {
        float cellSize =
            wListSquares[0].GetComponent<RectTransform>().rect.width * 0.87f; //pawn size is 87% of grid square
        GameObject pawn = Instantiate(wPawnElementPrefab, Vector3.zero, Quaternion.identity);
        pawn.transform.SetParent(wPawnContainer.transform, false);
        pawn.transform.localScale = Vector3.one;
        pawn.GetComponent<RectTransform>().sizeDelta = new Vector2(cellSize, cellSize);
        //pawn.GetComponent<RectTransform>().position = wListSquares[white ? wBaseIdWhite : wBaseIdBlack].transform.position;
        pawn.GetComponent<RectTransform>().position = white
            ? wWhiteBasePanelScript.wCenterPoint.transform.position
            : wBlackBasePanelScript.wCenterPoint.transform.position;
        //
        Src_UI_pawnElement srcPawn = pawn.GetComponent<Src_UI_pawnElement>();
        srcPawn.white = white;
        srcPawn.id = pawnId;
        srcPawn.boardSquareId = white ? wBaseIdWhite : wBaseIdBlack;
        srcPawn.row = white ? 0 : wBoardSize - 1;
        srcPawn.column = white ? 0 : wBoardSize - 1;
        //
        if (white) wListPawnsWhite[pawnId] = srcPawn;
        if (!white) wListPawnsBlack[pawnId] = srcPawn;
    }

    public void createGrid(int boardSize, int baseSize, int pawns)
    {
        wBoardSize = boardSize;
        wBaseSize = baseSize;
        wTotalPawns = pawns;
        wBaseIdWhite = 0;
        wBaseIdBlack = wBoardSize * wBoardSize - 1;
        //
        wGridContainer.GetComponent<GridLayoutGroup>().constraintCount = boardSize;
        RectTransform rt = (RectTransform) wGridContainer.transform;
        float cellSize = rt.rect.width / boardSize;
        wGridContainer.GetComponent<GridLayoutGroup>().cellSize = new Vector2(cellSize, cellSize);
        wListSquares = new List<Src_UI_gridElement>();
        //
        //float basePanelSize = 2 * cellSize;
        //wWhiteBasePanelScript.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(basePanelSize, basePanelSize);
        //wBlackBasePanelScript.parent.GetComponent<RectTransform>().sizeDelta = new Vector2(basePanelSize, basePanelSize);
        //wWhiteBasePanelScript.parent.GetComponent<RectTransform>().anchoredPosition = new Vector2(cellSize, cellSize);
        //wBlackBasePanelScript.parent.GetComponent<RectTransform>().anchoredPosition = new Vector2(-cellSize, -cellSize);
        //
        int line = 0;
        for (int i = 0; i < boardSize * boardSize; i++)
        {
            GameObject square = Instantiate(wGridElementPrefab, Vector3.zero, Quaternion.identity);
            square.name = "Square_" + i + "_" + line + "x" + (i % boardSize);
            //
            Image squareImg = square.GetComponent<Image>();
            // if ((line + i) % 2 == 1)
            //     squareImg.color = Color.white;
            // else
            //     squareImg.color = new Color(0.9f, 0.9f, 0.9f, 1f);
            //
            GameObject highlightImage = square.transform.Find("Highlight_Image").gameObject;
            //highlightImage.GetComponent<Image>().color = new Color(0.3f, 0.9f, 0.9f, 1.5f);
            highlightImage.SetActive(false);
            //
            // Button sqButton = square.GetComponent<Button>();
            // sqButton.onClick.AddListener(() =>
            // {
            //     Debug.Log("Square_" + i + "_" + line + "x" + (i % size));
            // });
            //
            Src_UI_gridElement srcGridElement = square.GetComponent<Src_UI_gridElement>();
            srcGridElement.setup(this);
            srcGridElement.id = i;
            srcGridElement.row = line;
            srcGridElement.column = i % boardSize;
            //
            int row = line;
            int column = i % boardSize;
            if (row < baseSize && column < baseSize)
            {
                //squareImg.sprite = Resources.Load<Sprite>("Png/sandTile");
                srcGridElement.isBase = true;
            }
            else if (row >= boardSize - baseSize && column >= boardSize - baseSize)
            {
                //squareImg.sprite = Resources.Load<Sprite>("Png/sandTile");
                //squareImg.color = new Color(0.8f, 0.8f, 0.8f, 1f);
                srcGridElement.isBase = true;
            }

            //
            square.transform.SetParent(wGridContainer.transform, false);
            square.transform.localScale = Vector3.one;
            wListSquares.Add(srcGridElement);
            //
            if (i % boardSize == boardSize - 1) line++;
        }
    }

    #endregion

    #region Tools

    private int getIdfromXY(int x, int y)
    {
        return x * wBoardSize + y;
    }

    private int[] getXYfromId(int cellId)
    {
        return new int[2] {cellId / wBoardSize, cellId % wBoardSize};
    }

    public bool isSquareWhiteBase(int squareId)
    {
        int[] sqXY = getXYfromId(squareId);
        return sqXY[0] < wBaseSize && sqXY[1] < wBaseSize;
    }

    public bool isSquareBlackBase(int squareId)
    {
        int[] sqXY = getXYfromId(squareId);
        return sqXY[0] >= (wBoardSize - wBaseSize) && sqXY[1] >= (wBoardSize - wBaseSize);
    }

    public bool isSquareBlackOrWhiteBase(int row, int col)
    {
        return (row >= (wBoardSize - wBaseSize) && col >= (wBoardSize - wBaseSize)) ||
               (row < wBaseSize && col < wBaseSize);
    }

    public bool isSquareBlackOrWhiteBase(int squareId)
    {
        int[] sqXY = getXYfromId(squareId);
        return (sqXY[0] >= (wBoardSize - wBaseSize) && sqXY[1] >= (wBoardSize - wBaseSize)) ||
               (sqXY[0] < wBaseSize && sqXY[1] < wBaseSize);
    }

    private Src_UI_pawnElement findPawnInGrid(int gridId)
    {
        for (int i = 0; i < wListPawnsWhite.Length; i++)
        {
            if (wListPawnsWhite[i].boardSquareId == gridId && wListPawnsWhite[i].alive)
                return wListPawnsWhite[i];
            if (wListPawnsBlack[i].boardSquareId == gridId && wListPawnsBlack[i].alive)
                return wListPawnsBlack[i];
        }

        return null;
    }

    private bool hasGridSquarePawn(int squareId)
    {
        for (int i = 0; i < wTotalPawns; i++)
        {
            if (wListPawnsWhite[i].boardSquareId == squareId && wListPawnsWhite[i].alive) return true;
            if (wListPawnsBlack[i].boardSquareId == squareId && wListPawnsBlack[i].alive) return true;
        }

        return false;
    }

    public void updateGrid_WB_PawnsInBase()
    {
        updateGrid_PawnsInBase(true);
        updateGrid_PawnsInBase(false);
    }

    public void updateGrid_PawnsInBase(bool white)
    {
        if (gameEnded) return;
        for (int i = 0; i < (white ? wListPawnsWhite.Length : wListPawnsBlack.Length); i++)
        {
            Src_UI_pawnElement srcPawn =
                (white ? wListPawnsWhite : wListPawnsBlack)[i].GetComponent<Src_UI_pawnElement>();
            if (srcPawn.boardSquareId == (white ? wBaseIdWhite : wBaseIdBlack) && srcPawn.alive)
            {
                Sequence seq = DOTween.Sequence();
                seq.Append(srcPawn.show());
                seq.Join(srcPawn.scaleMe(true));
                Debug.Log("updateGrid_PawnsInBase " + white + " " + i);
                break;
            }
        }
    }

    #endregion

    #region Pawn Move, Pawn Destroy

    private int lastMove;
    private bool gameEnded;

    public Sequence makePawnMovement(Src_UI_pawnElement movingPawn, int toId)
    {
        Sequence seq = DOTween.Sequence();
        if (lastMove == toId || gameEnded) return seq;
        lastMove = toId;
        Debug.Log("makePawnMovement " + movingPawn.white + " " + wListSquares[movingPawn.boardSquareId] + " -> " +
                  toId);

        if (isSquareBlackOrWhiteBase(toId))
            gameEnded = true; //game ended

        Src_UI_gridElement fromCell_Script = wListSquares[movingPawn.boardSquareId];
        Src_UI_gridElement toCell_Script = wListSquares[toId];

        if (movingPawn.white && toCell_Script.id == wBaseIdWhite) movingPawn.reset();
        if (!movingPawn.white && toCell_Script.id == wBaseIdBlack) movingPawn.reset();

        Vector3 moveToVect = wListSquares[toId].transform.position;
        if (isSquareBlackBase(toId) || isSquareWhiteBase(toId))
        {
            moveToVect = isSquareBlackBase(toId)
                ? wBlackBasePanelScript.wCenterPoint.transform.position
                : wWhiteBasePanelScript.wCenterPoint.transform.position;
        }

        seq.Append(movingPawn.parent.transform.DOMove(moveToVect, .333f).SetEase(Ease.InOutSine)
            .SetDelay(movingPawn.white ? 0 : .555f)
            .OnStart(
                () =>
                {
                    wAudioTransition.Play();
                    isSquareClickEnabled = false;
                    if (movingPawn.white) deHighlightSquares();
                    Debug.Log("move: " + movingPawn.id + " from: " + movingPawn.boardSquareId + " to: " + toId);
                    movingPawn.boardSquareId = toId;
                    int[] toXY = getXYfromId(toId);
                    movingPawn.row = toXY[0];
                    movingPawn.column = toXY[1];
                }
            )
            .OnComplete(
                () =>
                {
                    if (!isSquareBlackOrWhiteBase(toId))
                    {
                        updateGrid_PawnsInBase(movingPawn.white);
                    }

                    if (movingPawn.alive & movingPawn.wCanvasGroup.alpha == 0f)
                        movingPawn.wCanvasGroup.alpha = 1; // check
                    if (!movingPawn.white) getDebugPawnStatus();
                    isSquareClickEnabled = true;
                }
            ));
        return seq;
    }

    public void resetGridAndPawns()
    {
        Debug.Log("ResetGrid");
        gameEnded = false;
        //
        Sequence seq = DOTween.Sequence();
        //ExecutionQ.Enqueue(() =>
        {
            for (int i = 0; i < wListPawnsWhite.Length; i++)
            {
                //Reset white pawns
                wListPawnsWhite[i].boardSquareId = wListPawnsWhite[i].row = wListPawnsWhite[i].column = wBaseIdWhite;
                wListPawnsWhite[i].boardSquareId = wListPawnsWhite[i].row = wListPawnsWhite[i].column = wBaseIdWhite;
                //Reset black pawns
                wListPawnsBlack[i].boardSquareId = wBaseIdBlack;
                wListPawnsBlack[i].row = wListPawnsBlack[i].column = wBaseIdBlack / wBaseIdBlack;
                //
                seq.Join(wListPawnsWhite[i]
                    .resetToBeginingState(wWhiteBasePanelScript.wCenterPoint.transform.position));
                seq.Join(wListPawnsBlack[i]
                    .resetToBeginingState(wBlackBasePanelScript.wCenterPoint.transform.position));
            }

            seq.OnComplete(
                () =>
                {
                    updateGrid_WB_PawnsInBase();
                    deHighlightSquares();
                    wListSquares[wBaseIdWhite].wButton.interactable = true;
                    isSquareClickEnabled = true;
                });
        }
        ; //);
    }

    private void srvMoveDestroyPawn(bool white, int pawnId, int destPosition, Sequence seq)
    {
        ExecutionQ.Enqueue(() =>
        {
            Src_UI_pawnElement pawnToProccess = white ? wListPawnsWhite[pawnId] : wListPawnsBlack[pawnId];
            if (pawnToProccess.boardSquareId == -1) return;
            Src_UI_gridElement srcGrid = wListSquares[pawnToProccess.boardSquareId];

            //Game ends,destroy pawns and move in base
            if ((white && isSquareBlackBase(destPosition)) || (!white && isSquareWhiteBase(destPosition)))
            {
                deHighlightSquares();
                for (int i = 0; i < wListPawnsWhite.Length; i++)
                {
                    if (white && isSquareBlackBase(wListPawnsBlack[i].boardSquareId) &&
                        wListPawnsBlack[i].wCanvasGroup.alpha == 1)
                    {
                        seq.Append(wListPawnsBlack[i].destroyMe(true));
                    }

                    if (!white && isSquareWhiteBase(wListPawnsWhite[i].boardSquareId) &&
                        wListPawnsWhite[i].wCanvasGroup.alpha == 1)
                    {
                        seq.Append(wListPawnsWhite[i].destroyMe(true));
                    }
                }

                seq.Join(makePawnMovement(pawnToProccess, destPosition).SetDelay(.223f));
                seq.Join(pawnToProccess.scaleMe(true).SetDelay(.363f));
            }
            else
            {
                if (pawnToProccess.boardSquareId != wBaseIdWhite || pawnToProccess.boardSquareId != wBaseIdBlack)
                    seq.Join(pawnToProccess.scaleMe(false)).SetDelay(pawnToProccess.white ? 0.123f : .555f);
                
                if (srcGrid.id != destPosition)
                    seq.Join(makePawnMovement(pawnToProccess, destPosition));
            }
        });
    }

    #endregion

    #region Server Interactions

    private srcObj_GameState gs;

    internal void proccessGameState(GameStateResponse gameStateResponse)
    {
        Sequence seq = DOTween.Sequence();
        gs = gameStateResponse.state;

        int resetPawnsCounter = 0;
        for (int i = 0; i < gs.whitePawns.Count; i++)
            if (gs.whitePawns[i].position.xCoord == 0 && gs.whitePawns[i].position.yCoord == 0)
                resetPawnsCounter++;
        if (resetPawnsCounter == gs.whitePawns.Count) resetGridAndPawns();
        else
        {
            for (int i = 0; i < gs.whitePawns.Count; i++)
            {
                int tmpI = i;

                int destPosW = getIdfromXY(gs.whitePawns[tmpI].position.xCoord, gs.whitePawns[tmpI].position.yCoord);
                int destPosB = getIdfromXY(gs.blackPawns[tmpI].position.xCoord, gs.blackPawns[tmpI].position.yCoord);

                if (gs.turn == 1)
                {
                    if (wListPawnsWhite[tmpI].boardSquareId != destPosW && gs.whitePawns[tmpI].alive)
                    {
                        srvMoveDestroyPawn(true, tmpI, destPosW, seq);
                    }

                    if (wListPawnsBlack[tmpI].boardSquareId != destPosB && gs.blackPawns[tmpI].alive)
                    {
                       // seq.SetDelay(0.333f);
                        srvMoveDestroyPawn(!true, tmpI, destPosB, seq);
                    }
                }
                else if (gs.turn == 2)
                {
                    if (wListPawnsBlack[tmpI].boardSquareId != destPosB && gs.whitePawns[tmpI].alive)
                    {
                        //seq.SetDelay(0.333f);
                        srvMoveDestroyPawn(!true, tmpI, destPosB, seq);
                    }

                    if (wListPawnsWhite[tmpI].boardSquareId != destPosW && gs.blackPawns[tmpI].alive)
                    {
                        srvMoveDestroyPawn(true, tmpI, destPosW, seq);
                    }
                }

                if (!isSquareWhiteBase(tmpI) || !isSquareBlackBase(tmpI))
                {
                    //black
                    if (!gs.blackPawns[tmpI].alive && wListPawnsBlack[tmpI].alive)
                    {
                        //seq.Append(wListPawnsBlack[tmpI].destroyMe(false).SetDelay(gs.turn == 1 ? 0.133f : 0.444f));
                        seq.Append(wListPawnsBlack[tmpI].destroyMe(false));
                    }

                    //white
                    if (!gs.whitePawns[tmpI].alive && wListPawnsWhite[tmpI].alive)
                    {
                        //seq.Append(wListPawnsWhite[tmpI].destroyMe(false).SetDelay(gs.turn == 2 ? 0.133f : 0.444f));
                        seq.Append(wListPawnsWhite[tmpI].destroyMe(false));
                    }
                }
            }
        }
    }

    void SendUserMovePawnToServer(int pawnId, int toXCoord, int toYCoord)
    {
        //Debug.Log("MovePawnToServer" + pawnId + " " + toXCoord + " " + toYCoord);
    }

    #endregion

    #region tstBtns

    public void btn1()
    {
        //makePawnMovement(true, 0, 5, 5, 3, 3);
        //makePawnMovement(true, 1, 5, 5, 3, 3);

        //wListPawnsWhite[0].wCanvasGroup.alpha = 1;
        //wListPawnsWhite[1].wCanvasGroup.alpha = 1;
        //wListPawnsWhite[2].wCanvasGroup.alpha = 1;

        Sequence seq2 = DOTween.Sequence()
            .Append(wListPawnsWhite[0].show())
            .Append(wListPawnsWhite[0].parent.transform.DOMove(wListSquares[16].transform.position, .445f)
                .SetEase(Ease.InOutSine))
            .Append(wListPawnsWhite[1].show())
            .Append(wListPawnsWhite[1].parent.transform.DOMove(wListSquares[17].transform.position, .445f)
                .SetEase(Ease.InOutSine))
            .Append(wListPawnsWhite[2].show())
            .Append(wListPawnsWhite[2].parent.transform.DOMove(wListSquares[18].transform.position, .445f)
                .SetEase(Ease.InOutSine))
            .Append(wListPawnsWhite[2].destroyMe(wListPawnsWhite[2].boardSquareId == 0));
    }

    public void btn2()
    {
        Debug.Log(getDebugPawnStatus());
    }

    public string getDebugPawnStatus()
    {
        string ps = "-Drawn-\n";
        for (int i = 0; i < wTotalPawns; i++)
        {
            ps += "(" + wListPawnsWhite[i].id + ":" + (wListPawnsWhite[i].alive ? "t," : "f,") +
                  wListPawnsWhite[i].boardSquareId + ") ";
            ps += "(" + wListPawnsBlack[i].id + ":" + (wListPawnsBlack[i].alive ? "t," : "f,") +
                  wListPawnsBlack[i].boardSquareId + ")\n";
        }

        ps += "\n-Grid-\n";
        for (int i = 0; i < wListSquares.Count; i++)
        {
            if (hasGridSquarePawn(i)) ps += "(" + i + "," + "t" + ")\n";
        }

        //Server
        if (gs != null)
        {
            ps += "\n-Server-\n";
            for (int i = 0; i < gs.whitePawns.Count; i++)
            {
                ps += "(" + gs.whitePawns[i].id + ":" + (gs.whitePawns[i].alive ? "t," : "f,") +
                      getIdfromXY(gs.whitePawns[i].position.xCoord, gs.whitePawns[i].position.yCoord) + ") ";
                ps += "(" + gs.blackPawns[i].id + ":" + (gs.blackPawns[i].alive ? "t," : "f,") +
                      getIdfromXY(gs.blackPawns[i].position.xCoord, gs.blackPawns[i].position.yCoord) + ")\n";
            }
        }

        wDebugPawnStatus.text = ps;
        return ps;
    }

    public void btn3()
    {
    }

    #endregion
}