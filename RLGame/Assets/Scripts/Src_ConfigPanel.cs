﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Src_ConfigPanel : MonoBehaviour
{
    [SerializeField] GameObject parent;
    [SerializeField] Toggle localServerToggle;
    [SerializeField] Toggle remoteServerToggle;
    [SerializeField] List<Slider> slider;
    [SerializeField] List<Text> sliderValue;

    void Start()
    {
        slider[0].value = Src_Constants.miniMaxPlies;
        sliderValue[0].text = slider[0].value.ToString();
        //
        slider[1].value = (float) Math.Round(Src_Constants.eGreedy, 2);
        sliderValue[1].text = slider[1].value.ToString();
        //
        slider[2].value = (float) Math.Round(Src_Constants.gamma, 2);
        sliderValue[2].text = slider[2].value.ToString();
        //
        slider[3].value = (float) Math.Round(Src_Constants.lambda, 2);
        sliderValue[3].text = slider[3].value.ToString();
        //
        localServerToggle.isOn = Src_Constants.useLocalServer;
    }

    public void onLocalServerToggleValueChange(bool value)
    {
        Src_Constants.useLocalServer = true;
    }
    
    public void onRemoteServerToggleValueChange(bool value)
    {
        Src_Constants.useLocalServer = false;
    }
    
    public void onSliderChangeValue(Slider _slider)
    {
        float sliderValue2decimal = (float) Math.Round((double) _slider.value, 2);
        if (_slider == slider[0])
        {
            sliderValue[0].text = _slider.value.ToString(); //_slider.value.ToString("F2");
            PlayerPrefs.SetInt("minimaxDepth", (int) _slider.value);
            Src_Constants.eGreedy = _slider.value;
        }
        else if (_slider == slider[1])
        {
            sliderValue[1].text = sliderValue2decimal.ToString(); //_slider.value.ToString("F2");
            PlayerPrefs.SetFloat("eGreedy", sliderValue2decimal);
            Src_Constants.eGreedy = sliderValue2decimal;
        }
        else if (_slider == slider[2])
        {
            sliderValue[2].text = sliderValue2decimal.ToString(); //_slider.value.ToString("F2");
            PlayerPrefs.SetFloat("gamma", sliderValue2decimal);
            Src_Constants.gamma = sliderValue2decimal;
        }
        else if (_slider == slider[3])
        {
            sliderValue[3].text = sliderValue2decimal.ToString(); //_slider.value.ToString("F2");
            PlayerPrefs.SetFloat("lambda", sliderValue2decimal);
            Src_Constants.lambda = sliderValue2decimal;
        }
    }

    public void onResetValuesBtnClick()
    {
        Src_Constants.miniMaxPlies = 3;
        Src_Constants.eGreedy = 0.7f;
        Src_Constants.gamma = 0.95f;
        Src_Constants.lambda = 0.5f;
        Start();
    }

    public void onHidePanelBtnClick()
    {
        parent.SetActive(false);
    }
}