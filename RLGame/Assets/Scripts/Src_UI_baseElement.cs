﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class Src_UI_baseElement : MonoBehaviour
{
    public GameObject parent;
    public GameObject wCenterPoint;
    public Image wHighlightImage;
    public CanvasGroup wHighlightImageCanvasGroup;
    private Sequence wHighLightSequense;

    public int squareRowOppEntersBase = -1;
    public int squareColumnOppEntersBase = -1;

    void Start()
    {
        wHighlightImageCanvasGroup.alpha = 0;
    }

    public Tween highLight(bool highLight)
    {
        if (highLight)
        {
            return wHighlightImageCanvasGroup.DOFade(1, 0.333f).SetEase(Ease.OutSine)
                .OnStart(
                    () =>
                    {
                        //wHighlightImage.color = new Color(0.9962779f, 1, 0, 0.5f);
                        wHighlightImageCanvasGroup.alpha = 0;
                        wHighlightImage.gameObject.SetActive(true);
                    }
                ).OnComplete(() =>
                {
                    wHighLightSequense = DOTween.Sequence();
                    wHighLightSequense.Join(wHighlightImageCanvasGroup.DOFade(0.6f, 0.667f).SetEase(Ease.OutSine));
                    wHighLightSequense.Append(wHighlightImageCanvasGroup.DOFade(1, 0.667f).SetEase(Ease.OutSine));
                    wHighLightSequense.SetLoops(-1);
                    wHighLightSequense.Play();
                }
                );
        }
        else
        {
            return wHighlightImageCanvasGroup.DOFade(0, 0.123f).SetEase(Ease.OutSine)
                .OnStart(
                    () =>
                    {
                        squareRowOppEntersBase = squareColumnOppEntersBase = -1;
                        wHighLightSequense.Kill();
                    })
               .OnComplete(
                   () =>
                   {
                       wHighlightImage.gameObject.SetActive(false);
                   }
               );
        }
    }


}
