package gr.eap.RLGameEcoServer.game;

import gr.eap.RLGameEcoServer.player.Participant;

public class GameState {
    private int[] board;
    private Participant nextToPlay;
    private double eGreedy;
    private double gamma;
    private double lambda;

    public double geteGreedy() {
        return eGreedy;
    }

    public void seteGreedy(double eGreedy) {
        this.eGreedy = eGreedy;
    }

    public double getGamma() {
        return gamma;
    }

    public void setGamma(double gamma) {
        this.gamma = gamma;
    }

    public double getLambda() {
        return lambda;
    }

    public void setLambda(double lambda) {
        this.lambda = lambda;
    }

    public int[] getBoard() {
        return board;
    }

    public void setBoard(int[] board) {
        this.board = board;
    }

    public Participant getNextToPlay() {
        return nextToPlay;
    }

    public void setNextToPlay(Participant nextToPlay) {
        this.nextToPlay = nextToPlay;
    }

}
