��� ��
�������� �����������
�������� ��� �������� �����������: �������� ��������������

Server
-------------
��������� ��� ��� eclipse java project ��� �������� ��� ������ ��� ��� server ��� ��������������.
����� ���������� �� ������� ��������� ��� workspace ��� �� project "RLLib".
��� �� ������������ � server ������ �� ������������ ��� �� �������� ��� ������ settings:
 - Port:       � ����� ���� ����� �� ������ � server. ����� ������ (��� �����������) �� �������������� � ����� 80, ������ ���� ���� server ��� ������������ ��� web server.
 - dbLocation: � ��������� ��� MySQL server (����� ��������� �� ������� � ������� autoReconnect=true). �.�. jdbc:mysql://localhost/rl_game_eco_server?autoReconnect=true
 - userName:   �� User Name �� �� ����� � server ��������� ��� MySQL
 - password:   �� password �� �� ����� � server ��������� ��� MySQL

� MySQL ������ �� ���� ��� ���� �� ����� rl_game_eco_server ��� �� �������� ��� ������ ��� �������. ��� ������ MySQL script.sql ������� ��� ���������� script ��� �� ���������� ��� ����� ���������.
 
� ��� �������� ������ ��� ������ �����, ��������� ��� https://github.com/kyriakosgi/RLGameEcoServer

