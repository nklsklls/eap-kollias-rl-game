package gr.eap.RLGameEcoClient.comm;

import com.google.gson.*;
import gr.eap.RLGameEcoClient.player.Avatar;
import gr.eap.RLGameEcoClient.player.Member;
import gr.eap.RLGameEcoClient.player.Player;

import java.lang.reflect.Type;

public class PlayerDeserializer implements JsonDeserializer<Player> {

    @Override
    public Player deserialize(JsonElement arg0, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
        Gson gson = new Gson();
        JsonObject jobject = arg0.getAsJsonObject();
        if (jobject.get("isHuman").getAsBoolean()) {
            return (Player) gson.fromJson(jobject, Member.class);
        } else {
            return (Player) gson.fromJson(jobject, Avatar.class);
        }
    }


}
