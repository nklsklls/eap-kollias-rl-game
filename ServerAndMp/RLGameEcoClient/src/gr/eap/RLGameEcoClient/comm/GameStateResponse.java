package gr.eap.RLGameEcoClient.comm;

import gr.eap.RLGameEcoClient.Client;
import gr.eap.RLGameEcoClient.game.Move;
import gr.eap.RLGameEcoClient.player.Participant.Role;
import org.rlgame.gameplay.*;

import java.util.UUID;

public class GameStateResponse extends Response {
    private GameState state;
    private UUID gameUid;

    public UUID getGameUid() {
        return gameUid;
    }

    public GameState getState() {
        return state;
    }

    public GameStateResponse(GameState state, UUID gameUid) {
        this.state = state;
        this.gameUid = gameUid;
        this.setType("gr.eap.RLGameEcoServer.comm.GameStateResponse");
    }

    @Override
    public void process() {
        //When game state is deserialized, pawns are created without their boardSize and baseSize properties set
        //We will check if this is true and correct it
        if (getState().getWhitePawns()[0].getBoardSize() == 0) {
            for (byte i = 0; i < Client.currentNumberOfPawns; i++) {
                getState().getWhitePawns()[i].setBoardSize(Client.currentBoardSize);
                getState().getWhitePawns()[i].setBaseSize(Client.currentBaseSize);
                getState().getBlackPawns()[i].setBoardSize(Client.currentBoardSize);
                getState().getBlackPawns()[i].setBaseSize(Client.currentBaseSize);
            }
        }
        if (getState().isFinal()) {
            Client.machine.finishGameSession();
            Client.machine = null; //do this so as to set again the changed settings 4 lines below
        } else {
            //This is a temp fix, needs to be fixed better
            if (Client.machine == null) {
                Settings.setMpAlgorythmSettings(state.getPlayerType(), state.getMiniMaxPlies(), state.geteGreedy(), state.getGamma(), state.getLambda());
                if (state.getPlayerType() == 3)
                    Client.machine = new RandomPlayer(Settings.BLACK_PLAYER);
                else if (state.getPlayerType() == 2)
                    Client.machine = new MMPlayer(Settings.BLACK_PLAYER, Settings.PLAYER_B_PLIES, Settings.WHITE_PLAYER, (byte) state.getBoardSize(), (byte) state.getBaseSize(), (byte) state.getNumberOfPawns());
                else if (state.getPlayerType() == 1)
                    Client.machine = new RLPlayer(Settings.BLACK_PLAYER, (byte) state.getBoardSize(), (byte) state.getBaseSize(), (byte) state.getNumberOfPawns());
                else { //if playerType not set, use default type from settings file
                    String playerType = Client.clientSettings.getProperty("playerType");
                    switch (playerType) {
                        //Create your player class with your algorithm, by implementing the IPlayer interface
                        case "RANDOM_PLAYER":
                            Client.machine = new RandomPlayer(Settings.BLACK_PLAYER);
                            break;
                        case "MM_PLAYER":
                            Client.machine = new MMPlayer(Settings.BLACK_PLAYER, Settings.PLAYER_B_PLIES, Settings.WHITE_PLAYER, (byte) state.getBoardSize(), (byte) state.getBaseSize(), (byte) state.getNumberOfPawns());
                            break;
                        case "RL_PLAYER":
                            Client.machine = new RLPlayer(Settings.BLACK_PLAYER, (byte) state.getBoardSize(), (byte) state.getBaseSize(), (byte) state.getNumberOfPawns());
                            break;
                        default:
                            break;
                    }
                }
            }
            if (getState().getTurn() == Client.machine.getId()) {
                if (Client.joinRole != Role.SPECTATOR) {

                    Move pickedMove = Client.machine.pickMove(getState());

                    MoveCommand mc = new MoveCommand();
                    mc.setSocket(getSocket());
                    mc.setPawnId(pickedMove.getPawn().getId());
                    mc.setToXCoord(pickedMove.getToSquare().getXCoord());
                    mc.setToYCoord(pickedMove.getToSquare().getYCoord());
                    mc.setUserId(getUserId());
                    mc.setGameUid(getGameUid());
                    mc.send();
                } else {
                    Client.lastState = getState();
                }
            } else if (getState().getTurn() != Client.machine.getId() && (Client.joinRole == Role.SPECTATOR)) {
                Move observedMove = getState().findMoveFromLastState(Client.lastState);
                Client.machine.pickMove(getState(), observedMove);
            }

        }

    }


}
