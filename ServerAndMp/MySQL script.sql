CREATE DATABASE rl_game_eco_server;
USE rl_game_eco_server;

CREATE TABLE players (
 ID int(11) NOT NULL AUTO_INCREMENT,
 Name varchar(100) NOT NULL,
 Username varchar(20) NOT NULL,
 Password varchar(20) NOT NULL,
 Is_Human tinyint(1) NOT NULL,
 Owner_Player_ID int(11) DEFAULT NULL,
 Score int(11) NOT NULL,
 PRIMARY KEY (ID),
 CONSTRAINT fk1 FOREIGN KEY (Owner_Player_ID) REFERENCES players (ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE USER server_user IDENTIFIED BY 'myPassword';

grant usage on *.* to server_user@localhost identified by 'myPassword';
grant all privileges on rl_game_eco_server.* to server_user@localhost;
